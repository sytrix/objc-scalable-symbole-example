//
//  AppDelegate.h
//  TestUI
//
//  Created by Pierre Mazan on 23/08/2019.
//  Copyright © 2019 Pierre Mazan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

