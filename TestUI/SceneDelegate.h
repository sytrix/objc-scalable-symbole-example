//
//  SceneDelegate.h
//  TestUI
//
//  Created by Pierre Mazan on 23/08/2019.
//  Copyright © 2019 Pierre Mazan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

