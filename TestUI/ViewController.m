//
//  ViewController.m
//  TestUI
//
//  Created by Pierre Mazan on 23/08/2019.
//  Copyright © 2019 Pierre Mazan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void) loadView
{
    [super loadView];
    
    CGFloat padding = 20.f;
    CGFloat lineHeight = 50.f;
    CGFloat offsetY = 64.f;
    CGFloat imagePointSize = 20.f;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    NSMutableArray<NSLayoutConstraint*> *mutableArrayConstraint = [[NSMutableArray alloc] init];
    
    UIImage* image = [UIImage imageNamed:@"close_svg"];
    
    NSInteger nbDifferentWeight = 10;
    NSInteger nbDifferentScale = 4;
    for (NSInteger lineId = 1; lineId < nbDifferentWeight; lineId++) {
        for (NSInteger scaleId = 1; scaleId < nbDifferentScale; scaleId++) {
            UIImageSymbolConfiguration *symboleConfiguration = [UIImageSymbolConfiguration configurationWithPointSize:imagePointSize weight:(UIImageSymbolWeight)lineId scale:(UIImageSymbolScale)scaleId];
            
            UILabel *label = [[UILabel alloc] init];
            //[label setFont:[UIFont fontWithName:@"Helvetica" size:18]];
            label.translatesAutoresizingMaskIntoConstraints = NO;
            label.text = [NSString stringWithFormat:@"line : %ld", lineId];
            [scrollView addSubview:label];
            
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            imageView.translatesAutoresizingMaskIntoConstraints = NO;
            imageView.preferredSymbolConfiguration = symboleConfiguration;
            imageView.tintColor = [UIColor colorWithRed:((rand()%1000)/1000.f) green:((rand()%1000)/1000.f) blue:((rand()%1000)/1000.f) alpha:1.f];
            [scrollView addSubview:imageView];
            
            [mutableArrayConstraint addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(padding)-[label]-(padding)-|" options:0 metrics:@{@"padding":@(padding)} views:@{@"label":label}]];
            [mutableArrayConstraint addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(up)-[label]" options:0 metrics:@{@"up":@(offsetY+(padding+lineHeight)*(lineId-1))} views:@{@"label":label}]];
            [mutableArrayConstraint addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(left)-[imageView]" options:0 metrics:@{@"left":@(100+(scaleId-1)*50)} views:@{@"imageView":imageView}]];
            //[mutableArrayConstraint addObject:[label.firstBaselineAnchor constraintEqualToAnchor:imageView.firstBaselineAnchor]];
            [mutableArrayConstraint addObject:[label.lastBaselineAnchor constraintEqualToAnchor:imageView.lastBaselineAnchor]];
            
        }
    }
    scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, nbDifferentWeight * (lineHeight + padding) + offsetY);
    [self.view addSubview:scrollView];
    
    [NSLayoutConstraint activateConstraints:[NSArray arrayWithArray:mutableArrayConstraint]];

}

- (void) viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


@end
